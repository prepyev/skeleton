Шаблон для SEO-утилит
=====================

Разработчик, перед тем как написать свою утилиту убедись, что ее еще нет на https://packagist.org/

Требования
----------
* Все вспомигательные классы и функции должны быть в папке src/
* Один файл - один класс
* Используй [CamelCase](https://ru.wikipedia.org/wiki/CamelCase)
* Уважай [PSR12](https://www.php-fig.org/psr/psr-12/)
* Для CLI прлиложений используем fostam/getopts (уже подключен)

Форматирование кода
-------------------
Убедись, что код соответствует PSR12

```bash
$ composer run-script phpcs -- --standard=PSR12 app.php
```


Тесты
-----
Если по заданию полагаются тесты, запустить можно также через composer

```bash
$ composer run-script phpunit tests/exampleTest.php 
```
