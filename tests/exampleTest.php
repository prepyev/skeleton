<?php

require_once 'vendor/autoload.php';

use PHPUnit\Framework\TestCase;
use hardkod\skeleton\ExampleClass;


/**
 * Class ExampleTest
 */
class ExampleTest extends TestCase
{
    public function testFoo()
    {
        $class = new ExampleClass();
        $this->assertTrue($class->isFoo('Foo'));
    }
}