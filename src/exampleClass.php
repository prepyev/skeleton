<?php

namespace hardkod\skeleton;

/**
 * Class ExampleClass
 * @package hardkod\skeleton
 */
class ExampleClass
{
    /**
     * Пример метода
     *
     * @param string $var
     * @return bool
     */
    public function isFoo($var = "")
    {
        return $var == 'Foo';
    }
}
