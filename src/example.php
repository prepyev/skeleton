<?php

namespace hardkod\skeleton;

/**
 * Пример файла
 * PHP version 7
 *
 * @category  Tools
 * @package   HardkodSkeleton
 * @author    Hardkod <hello@hardkod.ru>
 * @copyright 2020 hardkod.ru
 * @license   https://opensource.org/licenses/BSD-2-Clause BSD-2-Clause
 * @link      https://www.hardkod.ru/
 */

/**
 * Пример функции
 *
 * @return string
 */
function exampleFunction()
{
    return 'Foo';
}
